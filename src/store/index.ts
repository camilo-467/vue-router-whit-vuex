import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        message : 'mensaje desde el store1',
        nombre : 'Juan',
        apellido : 'Corrales',
        persona: {
            'nombres' : '',
            'apellidos' : '',
            'correo' : '',
            'telefono' : '',
            'perfil' : ''
        },
        personas: [] as any,
    },
    mutations: {
        // aca van los metodos
        addPana (state, newUser) {
            state.personas = [...state.personas,newUser]
        }
    },
    actions: {
        addPanaAction ( context ) {
            context.commit('addPana');
        }
    },
    getters: {
        seeMessage (state) {
            return state.message;
        }
    }
})
